              ,---------------------------,
              |  /---------------------\  |
              | |                       | |             Group : Int1-7
              | |   Welcome             | |             Authors : Louise BOUREY - Titouan FONTAINE - Aïda LEMDANI
              | |     To  Our           | |
              | |         GraphTheory   | |
              | |            Project    | |
              |  \_____________________/  |
              |___________________________|
            ,---\_____     []     _______/------,
          /         /______________\           /|
        /___________________________________ /  | ___
        |                                   |   |    )
        |  _ _ _                 [-------]  |   |   (
        |  o o o                 [-------]  |  /    _)__
        |__________________________________ |/     /    /
    /-------------------------------------/|      (_)__/
  /-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/ /
/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/-/ /
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Some Explanations :

You will find all the test graphs that were originally in Graphical Form recoded in .txt files 
Thus, to simplify everything for you, they are in the same directory as the code !
You have to run the file : src/TestClass.java

  ____    _                _
 |  _ \  (_)  ___    ___  | |   __ _   _ __ ___     ___   _ __
 | | | | | | / __|  / __| | |  / _` | | '_ ` _ \   / _ \ | '__|
 | |_| | | | \__ \ | (__  | | | (_| | | | | | | | |  __/ | |
 |____/  |_| |___/  \___| |_|  \__,_| |_| |_| |_|  \___| |_|

About Packages : 
we tried our best to use as few as possible but for safety reason we had to add one that  we called 'GraphTheory'. 
About Traces : 
At each code execution, a trace will be produce/refreshed but don't worry it will not disturb its flow! 

This project is divided in 2 parts : 


   ___           _                      
  / _ \__ _ _ __| |_     ___  _ __   ___
 / /_)/ _` | '__| __|   / _ \| '_ \ / _ \
/ ___| (_| | |  | |_   | (_) | | | |  __/
\/    \__,_|_|   \__|   \___/|_| |_|\___|
                                        
You will have to chose the number corresponding to the graph you want to test
Our program will : 
Read the corresponding file and save the graph in memory
Display the adjacency matrix and the value matrix from memory
Detect if there is a cycle
If there is no cycle then it will calculate the ranks of the vertices and display them !!
You have now access to the second part :



 __      _              _       _ _             
/ _\ ___| |__   ___  __| |_   _| (_)_ __   __ _ 
\ \ / __| '_ \ / _ \/ _` | | | | | | '_ \ / _` |
_\ | (__| | | |  __| (_| | |_| | | | | | | (_| |
\__/\___|_| |_|\___|\__,_|\__,_|_|_|_| |_|\__, |
                                          |___/ 

Our program will : 
Have the same principle as the previous part except that we added some functions
Check if the graph is a scheduling graph throw many conditions and then,
If the graph is indeed a scheduling graph then it will compute earliest date and latest one in order
to display the calendars !!
