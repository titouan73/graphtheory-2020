package GraphTheory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


public class Bellman {
    Graph graph;
    ArrayList<int[]> minWeights; // Minimum path weights found between departure point to each vertex
    ArrayList<String[]> from;	 // List of predecessors enabling shortest path for each vertex
    ArrayList<int[]> infinity;	 // -1 shortest path found, 0 vertex reached, 1 vertex yet to be reached

    int fileNumber; // Not really beautiful but simplify the naming of trace files

    // Simple constructor
    public Bellman( Graph g, int fileNumber ){
        this.graph = g;
        this.fileNumber = fileNumber;
    }

    // Method to check if we ca use Bellman or not (= check negative edges presence)
    public void performBellman( int departure ) throws IOException {
        // If the trace file already exists, it erases previous content. Else it is created.
        String fileName = "trace" + fileNumber + "_" + departure;
        writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "BELLMAN\r\n\r\n", false );
        writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", graph.matrixToString( graph.adjacencyMatrixSorting(), "\r\n", "Adjacency" )+ "\r\n\r\n" , true );
        writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", graph.matrixToString( graph.valuesMatrixSorting(), "\r\n", "Values" ) + "\r\n\r\n" , true );

        // Initialization
        int nbVertices = graph.getNbOfVertices();

        minWeights = new ArrayList<int[]>();
        minWeights.add( new int[nbVertices] );
        for( int i=0; i<nbVertices; i++ ) minWeights.get(0)[i] = 0;

        from = new ArrayList<String[]>();
        from.add( new String[nbVertices] );
        for( int i=0; i<nbVertices; i++ ) from.get(0)[i] = "none";

        infinity = new ArrayList<int[]>();
        infinity.add( new int[nbVertices] );
        for( int i=0; i<nbVertices; i++ ) infinity.get(0)[i] = 1;


        // Departure is the shortest path to departure:
        infinity.get(0)[ departure ] = -1;

        from.get(0)[departure] = "" + departure;

        // List of vertices that were updated since previous iteration
        ArrayList<Integer> updatedVertices = new ArrayList<Integer>();
        updatedVertices.add(departure);

        String[][] valueMatrix = graph.valuesMatrixSorting();

        boolean finish = false;
        int n = 0;

        // Main section of the algorithm
        while( !finish ) {
            int index = minWeights.size();
            minWeights.add( new int[nbVertices] );
            infinity.add( new int[nbVertices] );
            from.add( new String[nbVertices] );

            for( int i=0; i<nbVertices; i++ ) {
                minWeights.get( index )[i] = minWeights.get( index-1 )[i];
                infinity.get( index )[i] = infinity.get( index-1 )[i];
                from.get( index )[i] = from.get( index-1 )[i];
            }


            // Update the successors of the vertices that were update during the previous iteration
            int prevSize = updatedVertices.size();
            for( int i=0; i<prevSize; i++) {
                for( int j=0; j<nbVertices; j++ ) {
                    String weightToSuccessor = valueMatrix[ updatedVertices.get(i)+1 ][ j+1 ];
                    // If we found an edge in the matrix (-> value != "-")
                    if( weightToSuccessor != "-" ) {
                        int newWeight = Integer.parseInt( weightToSuccessor )
                                + minWeights.get(index-1)[ updatedVertices.get(i) ];
                        // Infinity = 1 -> vertex not reached
                        // newWeight <= minWeight -> we found a new shortest path
                        if( infinity.get(index)[j] == 1 || newWeight <= minWeights.get(index)[j] ) {
                            // If there is no predecessors (from "none")
                            //we know the vertex was reach for the first time
                            if( from.get(index)[j].equals("none") )
                                from.get(index)[j] = "" + updatedVertices.get(i);
                            // if we found a new way with the same cost of the previous one, we keep both
                            if( newWeight == minWeights.get(index-1)[j] ) {
                                if( !from.get(index)[j].contains( "" + updatedVertices.get(i) ) )
                                    from.get(index)[j] += "or"+updatedVertices.get(i);
                            }
                            // else we replace the old one by the new one
                            else
                                from.get(index)[j] = "" + updatedVertices.get(i);
                            // Then we update the minimum weight
                            minWeights.get(index)[j] = newWeight;
                            infinity.get(index)[j] = 0;
                            updatedVertices.add(j);
                        }
                    }
                }
            }
            // We remove vertices that weren't updated since the previous iteration from updatedVertices list
            for(int i=0; i<prevSize; i++ ) updatedVertices.remove(0);

            n++;
            if( n == nbVertices ) {
                finish = true;
            }

            // Print the newly updated data
            System.out.println( bellmanToString("\n") );
            writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", bellmanToString("\r\n")+ "\r\n", true );
        }

        // Find if there is absorbent vertices or not
        boolean haveAbsorbantCycle = false;
        int lastLineIndex = from.size() - 1;
        for( int i=0; i<nbVertices; i++ ) {
            if( minWeights.get( lastLineIndex )[i] != minWeights.get( lastLineIndex - 1 )[i] ) {
                haveAbsorbantCycle = true;
                break;
            }
        }
        if( haveAbsorbantCycle ) {
            System.out.println( "The graph contains an absorbent cycle" );
            writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "The graph contains an absorbent cycle\r\n", true );
        }
        else {
            for( int i=0; i<nbVertices; i++ ) {
                // Print the shortest paths
                System.out.println("List of the shortest paths from " + departure + " to " + i + " :");
                writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "List of the shortest paths from " + departure + " to " + i + " :\r\n", true );
                ArrayList<Integer> shortestPath = new ArrayList<Integer>();
                printResult( departure, i, shortestPath );
                System.out.println();
                writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "\r\n", true );
            }
        }
    }

    // Recursive method to print the list of the shortest path and their weight (cost)
    private void printResult( int departure, int arrival, ArrayList<Integer> shortestPath ) throws IOException {
        String fileName = "trace" + fileNumber + "_" + departure;
        shortestPath.add( arrival );

        if( arrival == departure ) {
            for( int i=shortestPath.size()-1; i>=0; i-- ) {
                // Print complete path
                System.out.print( shortestPath.get(i) );
                writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "" + shortestPath.get(i), true );
                if( i>0 ) {
                    System.out.print(" -> ");
                    writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", " -> ", true );
                }
            }
            // Complete path weight
            int pathWeight = minWeights.get( minWeights.size()-1 )[ shortestPath.get(0) ];

            System.out.println();
            writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "\r\n", true );
            // Print complete path weight
            System.out.print("It weights " + pathWeight + "\r\n");
            writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt", "It weights " + pathWeight + "\r\n", true );
        }
        else {
            // Split in an array the list of shortest path predecessors of arrival point
            String[] predecessors = from.get( from.size() - 1 )[arrival].split("or");
            for( int i=0; i<predecessors.length; i++ ) {
                // if their is no predecessors, the point is inaccessible from departure point
                if( predecessors[i] == "none" ) {
                    System.out.println( arrival + " is innaccessible from " + departure);
                    writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt",arrival + " is innaccessible from " + departure + "\r\n", true );
                }
                // If the predecessor is already in the path, their is a loop (infinite number of shortest path, cost= 0?)
                else if( shortestPath.contains( Integer.parseInt( predecessors[i] ) ) ) {
                    System.out.println("There is a loop passing through " + Integer.parseInt( predecessors[i] ));
                    writeToFile( "src/GraphTheory/L3Int-1-" + fileName + ".txt",arrival + "There is a loop passing through " + Integer.parseInt( predecessors[i] ) + "\r\n", true );
                    return;
                }
                else {
                    // Recursive call (self call)
                    printResult( departure, Integer.parseInt( predecessors[i] ), shortestPath );
                }
            }
        }
        // Remove the vertex from shortest path once it was printed
        shortestPath.remove( shortestPath.size() - 1 );
    }

    // Method to open a file, write in it and then close it
    private void writeToFile( String filePath, String fileContent, boolean append ) throws IOException {
        FileWriter fWriter = new FileWriter( filePath, append );
        PrintWriter outputFile = new PrintWriter( fWriter );

        outputFile.print( fileContent );
        outputFile.close();
    }

    // Method to print Bellman step by step (array form)
    private String bellmanToString(String retourChar) {
        String str = new String("");
        String newChar = new String("");
        int maxChar = 0;
        int NumberOfDigits; // enable to adapt columns' size

        // To find the size needed for the columns
        for( int i=0; i<minWeights.size(); i++ ) {
            for( int j=0; j<minWeights.get(i).length; j++ ) {
                NumberOfDigits = String.valueOf( minWeights.get(i)[j] ).length();
                if( infinity.get(i)[j] == 0 ) NumberOfDigits += from.get(i)[j].length() + 2;
                if( NumberOfDigits > maxChar ) maxChar = NumberOfDigits;
            }
        }

        // Header (vertices names)
        str += "Iteration";
        for( int k="Iteration".length(); k<minWeights.get(0).length*2; k++ ) str += " ";
        str += "|";
        for( int i=0; i<minWeights.get(0).length; i++ ) {
            str += i;
            NumberOfDigits = String.valueOf( i ).length();
            for( int k=0; k < maxChar + 1 - NumberOfDigits; k++ ) str += " ";
        }
        str += retourChar ;
        for( int k=0; k<minWeights.get(0).length + 2 + "resolved".length(); k++ ) str += "-";
        for( int i=0; i<minWeights.get(0).length; i++ ) {
            for( int k=0; k < maxChar+1; k++ ) str += "-";
        }
        str += retourChar;

        // Body
        for( int i=1; i<minWeights.size(); i++ ) { // i=1 trick
            // Iteration column
            String newStr = "k=" + i;
            for( int c=newStr.length(); c<minWeights.get(0).length*2; c++ ) newStr += " ";
            newStr += "|";
            str += newStr;

            // Minimum Weights
            for( int j=0; j<minWeights.get(i).length; j++ ) {

                if ( infinity.get(i)[j] == 1 )
                    newChar = "inf";
                else {
                    newChar = "" + minWeights.get(i)[j] + "(" + from.get(i)[j] + ")";
                }

                str += newChar;
                NumberOfDigits = newChar.length();
                for( int k=0; k < maxChar + 1 - NumberOfDigits; k++ ) str += " ";
            }
            str += retourChar;
        }
        return str;
    }
}

