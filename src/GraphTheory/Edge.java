package GraphTheory;

public class Edge {
    /*----------------------ATTRIBUTES----------------------*/
   //DEFAULT WEIGHT value of an edge
    private final static int DEFAULT_WEIGHT = 0;

    //Number of edges created (incremented in the constructor)
    private static int nbOfEdges;

    private int weight = DEFAULT_WEIGHT;

    //ID of the instance/edge
    private int edgeID;

    //Vertex from which the Edge starts
    private Vertex startVertex;

    //Vertex to which the Edge goes
    private Vertex endVertex;
    /*-------------------END: ATTRIBUTES-------------------*/


    /*-----------------------METHODS-----------------------*/
    /*CONSTRUCTOR*/
    //Instanciate an edge that link two vertices
    public Edge(Vertex start, Vertex end){
        this.startVertex = start;
        this.endVertex = end;
        this.edgeID = nbOfEdges++;
    }
    //Instanciate an edge that link two vertices + weight
    public Edge(Vertex start, int weight, Vertex end){
        this(start, end);
        this.weight = weight;
    }
    /*END: CONSTRUCTOR*/

    /*GETTERS & SETTERS*/
    //Retrieve the weight value of the edge
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    //Retrieve the vertex at the origin of the edge
    public Vertex getStartVertex() {
        return startVertex;
    }

    public void setStartVertex(Vertex startVertex) {
        this.startVertex = startVertex;
    }

    //Retrieve the vertex at the end of the edge
    public Vertex getEndVertex() {
        return endVertex;
    }

    public void setEndVertex(Vertex endVertex) {
        this.endVertex = endVertex;
    }

    //Retrieve the number of edges currently created
    public static int getNbOfEdges() {
        return nbOfEdges;
    }

    //Retrieve the ID of the instance/edge
    public int getEdgeID() {
        return edgeID;
    }
    /*END: GETTERS & SETTERS*/

    /*OTHER METHODS*/


    /*END: OTHER METHODS*/

    /*--------------------END: METHODS--------------------*/
}
