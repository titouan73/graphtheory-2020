package GraphTheory;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import static GraphTheory.TestClass.writeToFile;

public class Graph {
    /*----------------------ATTRIBUTES----------------------*/
    private int nbOfVertices; //Number of vertices, retrieved in the first line of the file

    private boolean hasCycle;

    private ArrayList<Vertex> vertices; //All vertices

    private final String filePath;
    /*-------------------END: ATTRIBUTES-------------------*/


    /*-----------------------METHODS-----------------------*/
    /*CONSTRUCTOR*/
    public Graph(String filePath) throws InvalidFileException, IOException {
        //Reset all IDs of all the previously created vertices
        Vertex.resetIDs();
        //Saving filePath
        this.filePath = filePath;

        FileReader file = new FileReader(filePath);
        Scanner sc = new Scanner(file);

        readNumberOfVertices(sc);
        //Creation of an Arraylist vertices of size "number of vertices" containing objects of type Vertex
        this.vertices = new ArrayList<>();
        for (int i = 0; i < nbOfVertices; i++) {
            this.vertices.add(new Vertex());
        }
        //Going through all edges in the file and simultaneously adding them to the new instance
        while (sc.hasNextLine()) {
            this.addEdge(sc);
        }
        file.close();
        sc.close();
    }
    /*END: CONSTRUCTOR*/

    /*GETTERS & SETTERS*/
    //Retrieve the number of vertices read from the file
    public int getNbOfVertices() {
        return nbOfVertices;
    }

    public void setNbOfVertices(int nbOfVertices) {
        this.nbOfVertices = nbOfVertices;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(ArrayList<Vertex> vertices) {
        this.vertices = vertices;
    }

    /*END: GETTERS & SETTERS*/

    /*OTHER METHODS*/
    //Read and store the number of vertices in the graph
    public void readNumberOfVertices(Scanner sc) throws InvalidFileException {
        final int MINIMUM_NB_OF_VERTICES = 1;

        if (sc.hasNextInt()) {
            this.setNbOfVertices(sc.nextInt());

            if (this.getNbOfVertices() < MINIMUM_NB_OF_VERTICES) {
                throw new InvalidFileException("Error in the file: a graph must contain at least " + MINIMUM_NB_OF_VERTICES + "vertices not " + getNbOfVertices() + ", graph cannot be implemented from this file.");
            }
            if (sc.hasNextLine()) sc.nextLine();

        } else {
            throw new InvalidFileException("Error with the file: Cannot read the number of vertices, graph cannot be implemented from this file.");
        }
    }

    //Add an edge to the graph, based on information in the file
    private void addEdge(Scanner sc) throws InvalidFileException {
        Scanner sc2 = new Scanner(sc.nextLine());

        try {
            int start = sc2.nextInt(); //Reading start vertex
            int end = sc2.nextInt(); //Reading end vertex
            int weight = sc2.nextInt(); // Reading weight if the vertex


            //Verification of inputs
            if ((start < 0 || start >= this.nbOfVertices) || (end < 0 || end >= this.nbOfVertices)) {
                throw new InvalidFileException("Error in the file: a line describing an edge contains a fault in its parameters value, graph cannot be implemented from this file.");
            }

            //We add the edge to the graph
            Edge edge = new Edge(this.vertices.get(start), weight, this.vertices.get(end));
            this.vertices.get(start).addOutEdge(edge);
            this.vertices.get(end).addInEdge(edge);

        } catch (NoSuchElementException e) {
            throw new InvalidFileException("Error in the file: a line describing an edge contains a fault in its own structure (should be: start weight end), graph cannot be implemented from this file.");
        }
    }

    //Sort graph info into a 2D array representing the values matrix
    public String[][] valuesMatrixSorting() {

        String valuesMatrix[][] = new String[this.vertices.size() + 1][this.vertices.size() + 1];
        int start, weight, end;
        int line, column;

        valuesMatrix[0][0] = " ";
        for (line = 1; line < valuesMatrix.length; line++) {
            valuesMatrix[line][0] = String.valueOf(line - 1);
        }
        for (column = 1; column < valuesMatrix.length; column++) {
            valuesMatrix[0][column] = String.valueOf(column - 1);
        }

        for (int i = 0; i < this.vertices.size(); i++) {
            for (int j = 0; j < this.vertices.get(i).getOutEdges().size(); j++) {
                start = vertices.get(i).getOutEdges().get(j).getStartVertex().getVertexID();
                weight = vertices.get(i).getOutEdges().get(j).getWeight();
                end = vertices.get(i).getOutEdges().get(j).getEndVertex().getVertexID();
                middleloop:
                for (line = 1; line < valuesMatrix.length; line++) {
                    for (column = 1; column < valuesMatrix.length; column++) {
                        if ((start == line - 1) && (end == column - 1)) {
                            valuesMatrix[line][column] = String.valueOf(weight);
                            break middleloop;
                        }
                    }
                }
            }
        }
        for (line = 1; line < valuesMatrix.length; line++) {
            for (column = 1; column < valuesMatrix.length; column++) {
                if (valuesMatrix[line][column] == null) {
                    valuesMatrix[line][column] = "-";
                }
            }
        }
        return valuesMatrix;
    }

    //Sort graph info into a 2D array representing the adjacency matrix
    public String[][] adjacencyMatrixSorting() {

        String adjacencyMatrix[][] = new String[this.vertices.size() + 1][this.vertices.size() + 1];
        int start, end;
        int line, column;

        adjacencyMatrix[0][0] = " ";
        for (line = 1; line < adjacencyMatrix.length; line++) {
            adjacencyMatrix[line][0] = String.valueOf(line - 1);
        }
        for (column = 1; column < adjacencyMatrix.length; column++) {
            adjacencyMatrix[0][column] = String.valueOf(column - 1);
        }

        for (int i = 0; i < this.vertices.size(); i++) {
            for (int j = 0; j < this.vertices.get(i).getOutEdges().size(); j++) {
                start = vertices.get(i).getOutEdges().get(j).getStartVertex().getVertexID();
                end = vertices.get(i).getOutEdges().get(j).getEndVertex().getVertexID();
                middleloop:
                for (line = 1; line < adjacencyMatrix.length; line++) {
                    for (column = 1; column < adjacencyMatrix.length; column++) {
                        if ((start == line - 1) && (end == column - 1)) {
                            adjacencyMatrix[line][column] = String.valueOf(1);
                            break middleloop;
                        }
                    }
                }
            }
        }
        for (line = 1; line < adjacencyMatrix.length; line++) {
            for (column = 1; column < adjacencyMatrix.length; column++) {
                if (adjacencyMatrix[line][column] == null) {
                    adjacencyMatrix[line][column] = "-";
                }
            }
        }
        return adjacencyMatrix;
    }

    // Generate a string to display a matrix regardless of the size of the Strings inside
    public String matrixToString(String[][] myMatrix, String retourChar, String type) {
        String strFinal = type + " Matrix:" + retourChar;
        int line, column;

        // Find the maximum length of a cell to display
        int max = this.getNbOfVertices();
        for (int i = 0; i < this.getNbOfVertices(); i++) {
            Vertex vertex = this.getVertices().get(i);
            for (int j = 0; j < vertex.getInOutEdges().size(); j++) {
                int weight = vertex.getInOutEdges().get(j).getWeight();
                if (Math.abs(weight) > max) max = weight;
            }
        }
        int maxNumberOfDigits = String.valueOf(max).length() + 1 + 3;
        // +1 in case their is a negative sign
        // + 3 to be less packed
        int numberOfDigits;

        for (column = 0; column < myMatrix.length; column++) {
            numberOfDigits = String.valueOf(myMatrix[0][column]).length();
            for (int space = 0; space < maxNumberOfDigits - numberOfDigits; space++) strFinal += " ";
            strFinal += myMatrix[0][column];
        }
        strFinal += retourChar;

        for (line = 1; line < myMatrix.length; line++) {
            for (column = 0; column < myMatrix.length; column++) {
                numberOfDigits = String.valueOf(myMatrix[line][column]).length();
                for (int space = 0; space < maxNumberOfDigits - numberOfDigits; space++) strFinal += " ";
                strFinal += myMatrix[line][column];
            }
            strFinal += retourChar;
        }
        return strFinal;
    }

    public ArrayList<Integer> displayVertices(ArrayList<Vertex> example) {
        ArrayList<Integer> finalList = new ArrayList<>();
        for (int i = 0; i < example.size(); i++) {
            finalList.add(example.get(i).getVertexID());
        }
        return finalList;
    }

    public void displayEdges(ArrayList<Edge> example) {
        ArrayList<Integer> finalList = new ArrayList<>();
        for (int i = 0; i < example.size(); i++) {
            finalList.add(example.get(i).getEdgeID());
        }
        System.out.println(finalList);
    }


    public void ComputeRanks() {

        String[][] myMatrix = this.adjacencyMatrixSorting();

        int line, line2 = 1, column, column2 = 1, cpt = 0, rang = 0, supr = 0;
        boolean v = true;
        ArrayList<String> verif = new ArrayList<>();


        while (v) {
            //System.out.println("dans le while");
            //System.out.println(myMatrix.length);

            for (column = column2; column < myMatrix.length; column++) {
                cpt = 0;
                for (line = line2; line < myMatrix.length; line++) {

                    if (myMatrix[line][column].equals(String.valueOf(1))) {
                        cpt = 0;
                    }


                    if (myMatrix[line][column].equals("-")) {
                        cpt += 1;

                    }
                    if (cpt == (myMatrix.length - supr - 1)) {
                        verif.add(myMatrix[0][column]);

                    }


                }
            }
            if (!verif.isEmpty()) {


                for (String look : verif) {

                    this.getVertices().get(Integer.parseInt(look)).setRank(rang);

                    int swap = Integer.parseInt(look);
                    for (line = 1; line < myMatrix.length; line++) {
                        myMatrix[line][swap + 1] = "#";

                    }
                    for (column = 1; column < myMatrix.length; column++) {
                        myMatrix[swap + 1][column] = "#";


                    }
                    supr += 1;
                }
                verif.clear();


                rang += 1;
            }


            if (supr == myMatrix.length - 1) {
                v = false;
            }


        }


    }

    public void displayRank( String fileName) throws IOException {
        System.out.println("\r\n\r\nComputation of the rank for each vertex\n");
        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\r\n\r\nComputation of the rank for each vertex\n", true);


        for (Vertex i : this.getVertices()) {
            int look = i.getVertexID();
            int rang = i.getRank();
            System.out.println("The vertex " + look + " has a rank of " + rang);
            writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nThe vertex " + look + " has a rank of " + rang + "\n", true);

        }
        System.out.println("All ranks are now established");

        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\r\nAll ranks are now established\n", true);

    }

    public boolean hasCycle(String[][] myMatrix, String fileName) throws IOException {

        System.out.println("\nLooking if the graph is cyclic using adjacency matrix\n\n");
        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\r\n\r\nLooking if the graph is cyclic using adjacency matrix\n\n\n", true);

        int line, line2 = 1, column, column2 = 1, cpt, supr = 0, step = 0;
        boolean v = true;
        ArrayList<String> verif = new ArrayList<>();


        while (v) {


            for (line = line2; line < myMatrix.length; line++) {
                cpt = 0;
                for (column = column2; column < myMatrix.length; column++) {


                    if (myMatrix[line][column].equals(String.valueOf(1))) {

                        cpt = 0;

                        line2 = column;
                        if (verif.contains(myMatrix[0][column]) == false) {
                            verif.add(myMatrix[0][column]);
                        } else {
                            v = false;
                        }

                    }


                    if (myMatrix[line][column] == "-") {

                        cpt += 1;

                    }

                    if (cpt == (myMatrix.length - supr - 1)) {
                        System.out.println("Line " + (line - 1) + " is empty\nSo the column " + (line - 1) + " will be deleted");
                        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "Line " + (line - 1) + " is empty\nSo the column " + (line - 1) + " will be deleted\n\n", true);


                        column = line;
                        for (line = 1; line < myMatrix.length; line++) {
                            myMatrix[line][column] = "#";

                        }
                        line = column;

                        for (column = 1; column < myMatrix.length; column++) {
                            myMatrix[line][column] = "#";


                        }
                        column = line = 1;


                        supr += 1;
                        step++;
                        System.out.println(matrixToString(myMatrix, "\r\n", "Step " + step));
                        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", matrixToString(myMatrix, "\r\n", "Step " + step), true);


                        v = true;
                        verif.clear();
                    }
                }
            }

            if (supr == myMatrix.length - 2) {
                hasCycle = false;
                return hasCycle;
            }

        }
        hasCycle = true;
        return hasCycle;


    }

    public boolean CyclicOrNot(){
        return hasCycle;
    }





    /*END: OTHER METHODS*/

    /*--------------------END: METHODS--------------------*/

}
