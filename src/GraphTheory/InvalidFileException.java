package GraphTheory;

public class InvalidFileException extends Exception {
    private static final String DEFAULT_MESSAGE = "File contains structural error: it is impossible to draw a graph from this file";

    public InvalidFileException(){
        super(DEFAULT_MESSAGE);
    }

    public InvalidFileException(String message){
        super(message);
    }
}
