*** Begin of the Trace***

Adjacency Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     1     1     -     -     -     -     -     -     -
     1     -     -     1     -     1     1     -     -     -     -
     2     -     -     -     1     1     -     -     -     1     -
     3     -     -     -     -     -     -     -     1     -     1
     4     -     -     -     -     -     1     1     -     1     -
     5     -     -     -     -     -     -     1     1     -     -
     6     -     -     -     -     1     -     -     -     -     -
     7     -     -     -     -     -     -     -     -     1     -
     8     -     -     -     -     -     -     -     -     -     1
     9     -     -     -     -     -     -     -     -     -     -


Values Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     0     0     -     -     -     -     -     -     -
     1     -     -     2     -     2     2     -     -     -     -
     2     -     -     -     3     3     -     -     -     3     -
     3     -     -     -     -     -     -     -     5     -     5
     4     -     -     -     -     -     6     6     -     6     -
     5     -     -     -     -     -     -     2     2     -     -
     6     -     -     -     -     6     -     -     -     -     -
     7     -     -     -     -     -     -     -     -     6     -
     8     -     -     -     -     -     -     -     -     -     1
     9     -     -     -     -     -     -     -     -     -     -




Looking if the graph is cyclic using adjacency matrix


Line 9 is empty
So the column 9 will be deleted

Step 1 Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     1     1     -     -     -     -     -     -     #
     1     -     -     1     -     1     1     -     -     -     #
     2     -     -     -     1     1     -     -     -     1     #
     3     -     -     -     -     -     -     -     1     -     #
     4     -     -     -     -     -     1     1     -     1     #
     5     -     -     -     -     -     -     1     1     -     #
     6     -     -     -     -     1     -     -     -     -     #
     7     -     -     -     -     -     -     -     -     1     #
     8     -     -     -     -     -     -     -     -     -     #
     9     #     #     #     #     #     #     #     #     #     #
Line 8 is empty
So the column 8 will be deleted

Step 2 Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     1     1     -     -     -     -     -     #     #
     1     -     -     1     -     1     1     -     -     #     #
     2     -     -     -     1     1     -     -     -     #     #
     3     -     -     -     -     -     -     -     1     #     #
     4     -     -     -     -     -     1     1     -     #     #
     5     -     -     -     -     -     -     1     1     #     #
     6     -     -     -     -     1     -     -     -     #     #
     7     -     -     -     -     -     -     -     -     #     #
     8     #     #     #     #     #     #     #     #     #     #
     9     #     #     #     #     #     #     #     #     #     #
Line 7 is empty
So the column 7 will be deleted

Step 3 Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     1     1     -     -     -     -     #     #     #
     1     -     -     1     -     1     1     -     #     #     #
     2     -     -     -     1     1     -     -     #     #     #
     3     -     -     -     -     -     -     -     #     #     #
     4     -     -     -     -     -     1     1     #     #     #
     5     -     -     -     -     -     -     1     #     #     #
     6     -     -     -     -     1     -     -     #     #     #
     7     #     #     #     #     #     #     #     #     #     #
     8     #     #     #     #     #     #     #     #     #     #
     9     #     #     #     #     #     #     #     #     #     #
Line 3 is empty
So the column 3 will be deleted

Step 4 Matrix:
           0     1     2     3     4     5     6     7     8     9
     0     -     1     1     #     -     -     -     #     #     #
     1     -     -     1     #     1     1     -     #     #     #
     2     -     -     -     #     1     -     -     #     #     #
     3     #     #     #     #     #     #     #     #     #     #
     4     -     -     -     #     -     1     1     #     #     #
     5     -     -     -     #     -     -     1     #     #     #
     6     -     -     -     #     1     -     -     #     #     #
     7     #     #     #     #     #     #     #     #     #     #
     8     #     #     #     #     #     #     #     #     #     #
     9     #     #     #     #     #     #     #     #     #     #

The Graph is cyclic, So we can't compute the rank the vertices


--> Let's check if the Scheduling is available 
The Graph is unfortunately cyclic


We can't perform the Scheduling