package GraphTheory;

import javax.print.DocFlavor;
import java.io.IOException;
import java.util.*;
import java.lang.Math.*;

import static GraphTheory.TestClass.writeToFile;


public class Schedule {

    private Graph graph;
    private String fileName;

    public Schedule(Graph graph, String fileName) {

        this.graph = graph;
        this.fileName = fileName;
    }

    //method to check if we can use the scheduling
    public boolean weCanPerformScheduling() throws IOException {

        //We check the number of entry point and exit point
        int nbOfEntryPoint = 0;
        int nbOfExitPoint = 0;
        for (int i = 0; i < graph.getNbOfVertices(); i++) {
            Vertex vertex = graph.getVertices().get(i);
            //System.out.println(nbOfEntryPoint);
            if (!vertex.hasPredecessors()) {
                nbOfEntryPoint++;

                //check if the out edges of the entry point has weight 0

            } else if (!vertex.hasSuccessors()) {
                nbOfExitPoint++;

            }
        }
        for (int j = 0; j < graph.getVertices().get(0).getOutEdges().size(); j++) {
            Edge edge1 = graph.getVertices().get(0).getOutEdges().get(j);
            if (edge1.getWeight() != 0) {
                System.out.println("Err : Weight of the entry point edges are not equals to 0");
                writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nErr : Weight of the entry point edges are not equals to 0\n", true);

                return false;
            }
        }
        if (nbOfEntryPoint != 1 || nbOfExitPoint != 1) {
            System.out.println("Err : nbOfEntryPoint : " + nbOfEntryPoint + " or NbOfExitpoint : " + nbOfExitPoint);
            writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nErr : nbOfEntryPoint : " + nbOfEntryPoint + " or NbOfExitpoint : " + nbOfExitPoint + "\n", true);

            return false;
        }
        //we check if there is a cycle
        if (graph.CyclicOrNot()) {

            System.out.println("The Graph is unfortunately cyclic");
            writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nThe Graph is unfortunately cyclic\r\n\r\n", true);
            return false;

        }
        //Check for negative edge
        for (int i = 0; i < graph.getNbOfVertices() - 1; i++) {
            Vertex vertex = graph.getVertices().get(i);
            Edge firstEdge = vertex.getOutEdges().get(0);
            for (int j = 0; j < vertex.getOutEdges().size(); j++) {
                //there must be no negative arc
                Edge edge = vertex.getOutEdges().get(j);
                //all out going edges must have the same weight

                if (edge.getWeight() < 0 || edge.getWeight() != firstEdge.getWeight()) {


                    System.out.println("Err : Negative edge or different weight");
                    writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nErr : Negative edge or different weight\n", true);
                    return false;
                }
            }
        }


        return true;
    }


    public void displayMap(Map map) {
        Set<Map.Entry<?, ?>> myLabels = map.entrySet();
        if (myLabels.isEmpty()) {
            System.out.println("Your Schedule is empty\n");

        }
        else {
            for (Map.Entry<?, ?> lookAt : myLabels) {
                Object lookAtLabel = lookAt.getKey();
                Object lookAtContent = lookAt.getValue();
                System.out.print(lookAtLabel + " : " + lookAtContent + "\n");

            }
        }
    }

    public void performScheduling() throws IOException {
        //create a map with all vertices and their respect rank
        HashMap<Integer, Integer> rankVertex = new HashMap<>();
        for (Vertex i : graph.getVertices()) {
            rankVertex.put(i.getVertexID(), i.getRank());
        }
        System.out.println("[VertexID : Rank]");
        displayMap(rankVertex);
        HashMap<Integer, Integer> rankVertexSorted = sortByValue(rankVertex);
        System.out.println("Sorted by rank :");
        System.out.println("[VertexID : Rank]");

        displayMap(rankVertexSorted);
        SortedMap<Integer, Integer> earliestDates = new TreeMap<>();
        SortedMap<Integer, Integer> latestDates = new TreeMap<>();
        ArrayList<Vertex> vertices = graph.getVertices();

        //Set of the duration for all vertices
        for (Vertex i : vertices) {
            if (!i.getOutEdges().isEmpty()) {
                i.setDuration(i.getOutEdges().get(0).getWeight());

            } else {
                i.setDuration(i.getInEdges().get(0).getWeight());
                for (Edge e : i.getInEdges()) {
                    if (i.getDuration() > e.getWeight()) {
                        i.setDuration(e.getWeight());
                    }
                }

            }
        }

        //Begin of the earliest date computation
        //We put the entry point with a date of 0
        System.out.println("**Earliest Date**");
        for (int i : rankVertexSorted.keySet()) {


            earliestDates.put(i, earliestDate(vertices.get(i)));


        }
        System.out.println("[VertexID : Earliest Date]");
        displayMap(earliestDates);

        System.out.println("**Latest Date**");
        ArrayList<Integer> keys = new ArrayList<>(rankVertexSorted.keySet());
        for (int i = keys.size() - 1; i >= 0; i--) {


            latestDates.put(keys.get(i), latestDate(vertices.get(keys.get(i))));
        }
        System.out.println("[VertexID : Latest Date]");
        displayMap(latestDates);

        System.out.println("*** Final Schedule ***");
        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\n\n*** Final Schedule ***\n", true);



        System.out.println("RK | VX | ED | LD | TF | FF");
        writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nRK | VX | ED | LD | TF | FF\n\n", true);

        for (int i : rankVertexSorted.keySet()) {
            System.out.println(
                    rankVertexSorted.get(i) + " | "
                    + i + " | "
                    + earliestDates.get(i) + " | "
                    + latestDates.get(i) + " | "
                    + totalFloat(vertices.get(i)) + " | "
                    + freeFloat(vertices.get(i)));
            writeToFile("src/GraphTheory/L3Int1-7-" + fileName + ".txt",
                     rankVertexSorted.get(i) + " | "
                            + i + " | "
                            + earliestDates.get(i) + " | "
                            + latestDates.get(i) + " | "
                            + totalFloat(vertices.get(i)) + " | "
                            + freeFloat(vertices.get(i))+ "\n", true);

        }


    }

    public int earliestDate(Vertex vertex) {
        if (!vertex.hasPredecessors()) {
            return 0;
        }
        int ED = earliestDate(vertex.getPredecessors().get(0)) + vertex.getPredecessors().get(0).getDuration();
        for (Vertex i : vertex.getPredecessors()) {

            if (ED < (earliestDate(i) + i.getDuration())) {
                ED = (earliestDate(i) + i.getDuration());
            }
        }
        return ED;
    }

    public int latestDate(Vertex vertex) {
        if (!vertex.hasSuccessors()) {
            return earliestDate(vertex);
        }
        int LD = latestDate(vertex.getSuccessors().get(0)) - vertex.getDuration();
        for (Vertex i : vertex.getSuccessors()) {

            if (LD > (latestDate(i) - vertex.getDuration())) {
                LD = (latestDate(i) - vertex.getDuration());
            }
        }
        return LD;
    }

    public int totalFloat(Vertex vertex) {
        return latestDate(vertex) - earliestDate(vertex);
    }

    public int freeFloat(Vertex vertex) {
        // mL i = min(𝜏𝑗) − (𝜏𝑖 + 𝑑𝑖)
        if (!vertex.hasSuccessors()) {
            return 0;
        }
        int min = earliestDate(vertex.getSuccessors().get(0));
        for (Vertex j : vertex.getSuccessors()) {
            if (min > earliestDate(j)) {
                min = earliestDate(j);
            }

        }
        return min - (earliestDate(vertex) + vertex.getDuration());
    }

    // function to sort hashmap by values
    public static HashMap<Integer, Integer> sortByValue(HashMap<Integer, Integer> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<Integer, Integer>> list = new LinkedList<>(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<Integer, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<Integer, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }


}



