package GraphTheory;


import java.io.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TestClass {
    public static void main(String[] args) throws IOException {

        String answer = "No";
        do {
            try {
                //Ask the user to enter a number to open the corresponding graph file
            	boolean fileOk;
            	String str = "";
            	int fileNumber = 0;
            	int nbFile = nbFileInDirectory("src/GraphTheory");
            	do {
            		fileOk = true;
	                System.out.print("Please, enter the file number corresponding to the graph you want to implement (between 1 and " + nbFile + " included): ");
	                Scanner sc = new Scanner(System.in);
	                str = sc.nextLine();
	                
	            	// We check if the input is an integer or not
	            	Pattern p = Pattern.compile( "\\d+" );
	                Matcher m = p.matcher( str );
	                if( !m.find() ) {
	                	System.out.println( "Invalid input, please enter an integer between 1 and " + nbFile + " included.");
	                	fileOk = false;    	
	                }
	                else {
		                //Warning message if user enters a negative number or a number exceeding the number of file
		                fileNumber = Integer.parseInt(str);
		                if (fileNumber <= 0 || fileNumber > 13) {
		                	System.out.println("Error in user input stream format: only numbers between 1 and " + nbFile + " included are authorized.");
		                	fileOk = false;
		                }
	                }
                } while( !fileOk );
                
                //Graph creation, we pass the path of the file to the class constructor to save it
                Graph myGraph = new Graph("src/GraphTheory/L3Int1-7-graph" + str + ".txt");
                //Display of values Matrix
                System.out.println( myGraph.matrixToString( myGraph.valuesMatrixSorting(), "\n", "Values" ) );
                //Display of Adjacency Matrix
                System.out.println( myGraph.matrixToString( myGraph.adjacencyMatrixSorting(), "\r\n", "Adjacency"  ) );

                // If the trace file already exists, it erases previous content. Else it is created.
                String fileName = "trace" + fileNumber;
				writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "*** Begin of the Trace***\r\n\r\n", false );
				writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", myGraph.matrixToString( myGraph.adjacencyMatrixSorting(), "\r\n", "Adjacency" )+ "\r\n\r\n" , true );
				writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", myGraph.matrixToString( myGraph.valuesMatrixSorting(), "\r\n", "Values" ) + "\r\n\r\n" , true );

				System.out.println();
				Dijkstra d = new Dijkstra(myGraph, fileNumber);
				if( d.weCanUseDijkstra() ) {
					System.out.println("Do you want to use Dijkstra or Bellman algorithm?");
					System.out.println("1 - Dijkstra");
					System.out.println("2 - Bellman");

					Scanner keyboardInput = new Scanner( System.in );
					String choice;
					boolean ok;
					do {
						ok = true;
						choice = keyboardInput.next();

						if(  !choice.equals("1") && !choice.equals("2")) {
							System.out.println( "Invalid input, please choose 1 to perform Dijkstra or 2 to perform Belmann." );
							ok = false;
						}
					} while ( !ok );

					if( choice.equals("1") ) {
						System.out.println( "Dijkstra algorithm :" );
						int departure = askDeparture( myGraph );
						d.performDijkstra( departure );
					}
					else {
						System.out.println( "Bellman algorithm :" );
						int departure = askDeparture( myGraph );
						Bellman b = new Bellman( myGraph, fileNumber );
						b.performBellman( departure );
					}
				}
				else {
					System.out.println("We cannot use Dijkstra. We will use Bellman's algorithm:");
					int departure = askDeparture( myGraph );
					Bellman b = new Bellman( myGraph, fileNumber );
					b.performBellman( departure );
				}
				boolean hasCycle = myGraph.hasCycle(myGraph.adjacencyMatrixSorting(),fileName);

				if(!hasCycle){

					System.out.println("The Graph is not cyclic, So we can compute the rank the vertices\n\n");
					writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nThe Graph is not cyclic, So we can compute the rank the vertices\n\n", true );
					myGraph.ComputeRanks();
					myGraph.displayRank(fileName);



				}
				else {
					System.out.println("The Graph is cyclic, So we can't compute the rank the vertices\n\n");
					writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nThe Graph is cyclic, So we can't compute the rank the vertices\n\n", true );

				}
				System.out.println("\n\n --> Let's check if the Scheduling is available");
                writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\n--> Let's check if the Scheduling is available ", true );

                Schedule mySchedule = new Schedule(myGraph,fileName);
				if(mySchedule.weCanPerformScheduling()){
					System.out.println("\nWe can perform the Scheduling");
                    writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\n\nWe can perform the Scheduling", true );

                    mySchedule.performScheduling();

				}
				else {
					System.out.println("\nWe can't perform the Scheduling");
                    writeToFile( "src/GraphTheory/L3Int1-7-" + fileName + ".txt", "\nWe can't perform the Scheduling", true );

                }

				//Ask the user at the end if he wants to try the program with another graph file or exit it
				System.out.println("\n\nDo you want to test another graph ? Yes/No");
				Scanner sc2 = new Scanner(System.in);
				answer = sc2.nextLine().toLowerCase();

				while (!answer.equals("yes") && !answer.equals("no")){
					System.out.println("Invalid input, please enter yes or no");
					answer = sc2.nextLine().toLowerCase();
				}

				//If corresponding file is not found
			} catch (FileNotFoundException | InvalidFileException e) {
				e.printStackTrace();
			}
		} while (!answer.equals("no")) ;
	}



	// Function to ask the departure point to the user and return it
	public static int askDeparture( Graph myGraph ) {
		System.out.println( "Which departure point? Please enter a number in [0;" + myGraph.getNbOfVertices() + "["  );
		String departureStr;
		int departure = 0;
		boolean ok;
		Scanner keyboardInput = new Scanner( System.in );

		do {
			ok = true;
			departureStr = keyboardInput.next();

			// We check if the input is an integer or not
			Pattern p = Pattern.compile( "\\d+" );
			Matcher m = p.matcher( departureStr );
			if( !m.find() ) {
				System.out.println( "Invalid departure point, please enter an integer." );
				ok = false;
			}
			// if it is an integer, we checked it is an existing vertex
			else {
				departure = Integer.parseInt( departureStr );
				if( departure < 0 || departure >= myGraph.getNbOfVertices() ) {
					System.out.println( "Departure point out of the range [0;" + myGraph.getNbOfVertices() + "[. Please choose again." );
					ok = false;
				}
			}
		} while ( !ok );

		return departure;
	}



	// Method to open a file, write in it and then close it
	public static void writeToFile( String filePath, String fileContent, boolean append ) throws IOException {
		FileWriter fWriter = new FileWriter( filePath, append );
		PrintWriter outputFile = new PrintWriter( fWriter );

		outputFile.print( fileContent );
		outputFile.close();
	}


    // Method to count the number of file in a directory
    public static int nbFileInDirectory( String directoryPath ) {
        File f = new File(directoryPath);
        int count = 0;
    	Pattern p = Pattern.compile( "L3Int1-7-graph\\d+" );
    	String fileName;
    	
		for( int i=0; i<f.list().length; i++) {
			fileName = f.list()[i];
            Matcher m = p.matcher( fileName );
            if( m.find() ) count++;
		}
        System.out.println("Number of graph files: " + count);
        return count;
    }
}
