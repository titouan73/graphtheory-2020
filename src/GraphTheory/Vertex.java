package GraphTheory;

import java.util.*;

public class Vertex implements List<Integer> {
    /*----------------------ATTRIBUTES----------------------*/
    //ID of the vertex
    private int vertexID;

    //Count the number of vertices currently created
    private static int nbOfVertices;

    //List all the edges pointing toward the instance (entering)
    private ArrayList<Edge> inEdges = new ArrayList<>();

    //List all the edges departing from the instance (leaving)
    private ArrayList<Edge> outEdges = new ArrayList<>();

    private int rank;

    private int duration;
    /*-------------------END: ATTRIBUTES-------------------*/


    /*-----------------------METHODS-----------------------*/
    /*CONSTRUCTOR*/
    public Vertex(){
        this.vertexID = nbOfVertices++;
    }
    /*END: CONSTRUCTOR*/

    /*GETTERS & SETTERS*/

    public ArrayList<Edge> getInEdges() {
        return inEdges;
    }

    public ArrayList<Edge> getOutEdges() {
        return outEdges;
    }

    //Get the ID of the current instance
    public int getVertexID() {
        return vertexID;
    }

    public static int getNbOfVertices() {
        return nbOfVertices;
    }

    public void setRank(int rank){
        this.rank = rank;
    }

    public int getRank(){
        return rank;
    }

    public void setDuration(int duration){
        this.duration = duration;
    }

    public int getDuration(){
        return duration;
    }


    /*END: GETTERS & SETTERS*/

    /*OTHER METHODS*/

    //Reset all the class IDs, only used when a new graph is generated
    public static void resetIDs() {
        Vertex.nbOfVertices = 0;
    }

    //Add an incoming edge to the instance
    public void addInEdge(Edge edge) {
        this.inEdges.add(edge);
    }

    //Add an outgoing edge of the instance
    public void addOutEdge(Edge edge) {
        this.outEdges.add(edge);
    }

    //Get all the edges entering or leaving the current vertex/instance
    public ArrayList<Edge> getInOutEdges() {
        ArrayList<Edge> edges = new ArrayList<>(this.inEdges);
        Edge e;
        for(int i = 0; i < outEdges.size(); i++) {
            e = outEdges.get(i);
            if(!edges.contains(e)) {
                edges.add(e);
            }
        }
        return edges;
    }

    //List all the vertices which are predecessors of the current instance/vertex
    public ArrayList<Vertex> getPredecessors() {
        ArrayList<Vertex> predecessors = new ArrayList<>();
        Edge e;
        for(int i = 0; i < this.inEdges.size(); i++) {
            e = inEdges.get(i);
            if(!predecessors.contains(e.getStartVertex())) {
                predecessors.add(e.getStartVertex());
            }
        }
        return predecessors;
    }



    //List all the vertices which are successors of the current instance/vertex
    public ArrayList<Vertex> getSuccessors(){
        ArrayList<Vertex> successors = new ArrayList<>();
        Edge e;
        for(int i = 0; i < this.outEdges.size(); i++) {
            e = outEdges.get(i);
            if(!successors.contains(e.getEndVertex())) {
                successors.add(e.getEndVertex());
            }
        }
        return successors;
    }

    //Checks if the current vertex has at least one predecessor
    public boolean hasPredecessors(){
        if(this.inEdges.isEmpty()){
            return false;
        }
        return true;
    }

    //Checks if the current vertex has at least one successor
    public boolean hasSuccessors(){
        if(this.outEdges.isEmpty()){
            return false;
        }
        return true;
    }


    //Checks if the passed vertex (argument line) is a predecessor of the current instance/vertex
    public boolean isPredecessor(Vertex predecessor){
            Edge e;
            for(int i = 0; i < this.inEdges.size(); i++) {
                e = inEdges.get(i);
                if(!e.getStartVertex().equals(predecessor)) {
                    return true;
                }
            }
            return false;
    }
    //Checks if the passed vertex (argument line) is a successor of the current instance/vertex
    public boolean isSuccessor(Vertex successor){
        Edge e;
        for(int i = 0; i < this.outEdges.size(); i++) {
            e = outEdges.get(i);
            if(!e.getEndVertex().equals(successor)) {
                return true;
            }
        }
        return false;
    }

    //Checks if the passed vertex (argument line) is somehow linked to the current instance/vertex
    public boolean isLinked(Vertex vertex){
        return this.isPredecessor(vertex) || this.isSuccessor(vertex);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends Integer> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public Integer get(int i) {
        return null;
    }

    @Override
    public Integer set(int i, Integer integer) {
        return null;
    }

    @Override
    public void add(int i, Integer integer) {

    }

    @Override
    public Integer remove(int i) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int i) {
        return null;
    }

    @Override
    public List<Integer> subList(int i, int i1) {
        return null;
    }
    /*END: OTHER METHODS*/

    /*--------------------END: METHODS--------------------*/
}
