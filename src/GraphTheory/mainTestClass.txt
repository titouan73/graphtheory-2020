
The main class will be used as narrative thread of our program,
It contains a lot of displaying things so it needs IOException,

As many interactive program it's based on a DOwhile loop.
Inside this loop the first command who appears is a DOwhile (line17) loop used to choose the asked graph
It first detect the integer in the answer (line31)
And then verify that the integer is between 1 and the nb of files (line40)

Then we create a new graph of type Graph thanks to the constructor Graph(filepath)
Next to that we directly display in the console and in the trace the value Matrix and
the adjacency Matrix
using : both computation method valueMatrixSorting() and adjacencyMatrixSorting()
AND the display method matrixToString() (line 50 - 52)

Then we need to determine if yes or not there is a cycle in the graph
So using the boolean method hasCycle() and an If clause we return to the user if the program will
continue or not.

Indeed if there is no cycle we can compute the rank of each vertex.
So first we calculate it with the ComputeRank() method, and then we diplay it
using the displayRanks() method.

We have now to check if we are able to perform the Scheduling
--> weCanPerformScheduling() method
If all the clauses are ok that way we use the : performScheduling() method
That's the end of the performance.

Finally we ask the user if he want to test another graph
We treat his answer with a toLowerCase() in order to be more generic.

