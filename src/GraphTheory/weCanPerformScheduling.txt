
In order to perform the scheduling we must be sure that our graph
include a certain number of criteria.

We go throw the IOException because we use FileName in the function writeToFile
that allow us to feed the trace.

We begin with verification of the number of entry point AND exit point (line28)
We go throws the vertices and for each we check if it has no predecessor (entry point)
or no successor (exit point).

For each entry point we check that his outEdges are 0 weighted. (line41)
Then we check that we found only 1 entry point and only 1 exit point. (line50)

Next we use the method CyclicOrNot() to determine if there are cycle or not. (line57)

The last verification is for negative edge and different weighted edges. (line64)
Here we go throw all vertices except the exit point, and throw their outEdges we check if their weight
are equals and positive.

its only at the end that we can return true to our boolean method.
in other case it will return false and specify in the console and in the trace we the
problem appears.



